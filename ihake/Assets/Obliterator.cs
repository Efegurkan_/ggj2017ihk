﻿using UnityEngine;
using System.Collections;
using EZ_Pooling;

public class Obliterator : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "obstacle")
            EZ_PoolManager.Despawn(col.transform);
    }

    void Start()
    {
        Invoke("ReadjustCollider", 1f);
    }

    void ReadjustCollider()
    {
        GetComponent<BoxCollider2D>().size = new Vector2(9f,8f);
    }
}
