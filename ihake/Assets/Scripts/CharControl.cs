﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharControl : MonoBehaviour
{

    public float voice_note;
    public float normalized, max, min;
    public float interval;
    public static float height;
    private int n_steps;
    private int ceiling = 11;
    private int floor = 14;


    // Use this for initialization
    void Start()
    {
        max = 2000;
        min = 0;
        interval = (max - min) / n_steps;
        n_steps = ceiling + floor;
    }

    // Update is called once per frame
    void Update()
    {
        voice_note = GetComponentInChildren<AudioStuff>().final_note;

        if (Input.GetKey(KeyCode.H))
        {   // get high value.
            max = voice_note;
            interval = (max - min) / n_steps;
        }
        if (Input.GetKey(KeyCode.L))
        {   // get low value.
            min = voice_note;
            interval = (max - min) / n_steps;
        }

        if (voice_note > max)
        {
            voice_note = max;
        }
        if (voice_note < min)
        {
            voice_note = min;
        }

        height = ((voice_note - min) / interval) - floor;

        //height = (voice_note / 10) - 70f;

        //movement.Set(0, height, 0);
        //gameObject.GetComponent <Rigidbody> ().MovePosition (movement);
        //transform.position = movement;

    }
}