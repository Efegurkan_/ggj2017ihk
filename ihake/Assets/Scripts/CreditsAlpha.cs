﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class CreditsAlpha : MonoBehaviour {

    public float howManySecs;

	void Start () {

        Invoke("Credits",howManySecs);
	
	}

    void Credits()
    {
        GetComponent<Image>().DOFade(0f, 1f).OnComplete<Tweener>(() => Destroy(gameObject));
    }
}
