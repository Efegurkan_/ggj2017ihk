﻿using UnityEngine;
using EZ_Pooling;
using EazyTools.SoundManager;

public class PickupScript : MonoBehaviour {

    public AudioClip pickup;

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            SoundManager.PlaySound(pickup);
            EZ_PoolManager.Despawn(transform);
            GameManager.instance.Score++;
        }
    }
}
