﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LineWave : MonoBehaviour {

    float tweenTime = 0.1f;
    WaitForSeconds wfs;
    public int floatingTo;
    bool isMoving;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(0f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(1f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(2f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(3f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(4f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(5f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(6f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(7f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(8f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).DOMoveY(9f, tweenTime);
                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
        else
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (float.IsNaN(CharControl.height))
                    continue;

                transform.GetChild(i).DOMoveY(CharControl.height, tweenTime);

                tweenTime += 0.1f;
            }

            tweenTime = 0.1f;
        }
    }


}