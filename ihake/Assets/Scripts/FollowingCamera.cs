﻿using UnityEngine;
using System.Collections;

public class FollowingCamera : MonoBehaviour
{

    public static FollowingCamera instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    public Transform target;
    public Vector3 offset;

    public bool follow_x;
    public bool follow_y;
    public bool follow_z;

    Camera cam;

    private Bounds camBound;


    void Start()
    {
        cam = GetComponent<Camera>();
        OrthographicBounds();

    }

    void LateUpdate()
    {

        if (target != null && target.position + offset != transform.position)
        {
            Vector3 currentPos = transform.position;

            if (follow_x)
                currentPos.x = target.position.x + offset.x;
            if (follow_y)
                currentPos.y = target.position.y + offset.y;
            if (follow_z)
                currentPos.z = target.position.z + offset.z;

            transform.position = currentPos;
        }
        camBound.center = transform.position;
    }

    public Vector3 GetPositionAt2D()
    {
        Vector3 pos = transform.position;
        pos.z = 0;
        return pos;
    }

    public void OrthographicBounds()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = cam.orthographicSize * 2;
        camBound = new Bounds(cam.transform.position, new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
    }


    public bool IsInsideOfCamera2D(Vector3 pos)
    {

        return !(pos.x > camBound.max.x || pos.x < camBound.min.x || pos.y > camBound.max.y || pos.y < camBound.min.y);
    }


}