﻿using UnityEngine;
using EZ_Pooling;
using EZCameraShake;
using EazyTools.SoundManager;

public class EnemyScript : MonoBehaviour
{

    public AudioClip hit;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && !GameManager.instance.isInvincible)
        {
            //Die
            SoundManager.PlaySound(hit);
            CameraShaker.Instance.ShakeOnce(8f,8f,0.1f,0.3f);
            GameManager.instance.PlayerDied();
        }
    }
}