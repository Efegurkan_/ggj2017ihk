﻿using UnityEngine;
using System.Collections;
using EZ_Pooling;
using UnityEngine.UI;
using EazyTools.SoundManager;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool hasGameStarted = false;
    public float gameStartingTime = 0f;

    public GameObject[] towersFlipped;
    public GameObject[] towers;
    public GameObject floatingTower;
    public GameObject pickup;

    public GameObject cube;

    public Text scoreText;
    public Text gameOverScoreText;
    public Text livesText;

    public int lives = 3;
    Vector3 playerInitialPos;
    Vector3 carpetInitialPos;

    public GameObject player;

    public bool isInvincible = false;

    public AudioClip bgMusic;

    public GameObject noteAnimation;

    public Image[] youDied;

    private int score;
    public int Score
    {
        set
        {
            score = value;

            scoreText.text = score.ToString();
            gameOverScoreText.text = score.ToString();
        }
        get
        {
            return score;
        }
    }

    private static GameManager _instance;

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    void Start()
    {
        SoundManager.PlayMusic(bgMusic, 10f, true, false);
        score = 0;

        playerInitialPos.Set(11f, 1.5f, 0f);
        carpetInitialPos.Set(15f, 0f, 0f);

    }

    void Update()
    {
        if (AudioStuff.play_notes)
            noteAnimation.SetActive(true);

        else
            noteAnimation.SetActive(false);
    }

    public void StartGame()
    {
        gameStartingTime = Time.time;
        StartCoroutine(CreateTowersFlipped());
        StartCoroutine(CreateTowers());
        StartCoroutine(CreatePickups());
        StartCoroutine(CreateFloatingTower());
    }

    IEnumerator CreateTowersFlipped()
    {
        yield return new WaitForSeconds(Random.Range(2f, 7f));

        EZ_PoolManager.Spawn(towersFlipped[Random.Range(0, 3)].transform, new Vector3(60f, Random.Range(10f, 13.9f), 0), Quaternion.identity);

        yield return new WaitForSeconds(Random.Range(2f, 7f));

        StartCoroutine(CreateTowersFlipped());

        yield return new WaitForEndOfFrame();

    }

    IEnumerator CreateFloatingTower()
    {
        yield return new WaitForSeconds(Random.Range(20f, 30f));

        EZ_PoolManager.Spawn(floatingTower.transform, new Vector3(60f, -0.5f, 0), Quaternion.identity);

        yield return new WaitForSeconds(Random.Range(20f, 30f));

        StartCoroutine(CreateFloatingTower());

        yield return new WaitForEndOfFrame();

    }

    IEnumerator CreateTowers()
    {
        yield return new WaitForSeconds(Random.Range(2f, 7f));

        EZ_PoolManager.Spawn(towers[Random.Range(0, 3)].transform, new Vector3(60f, Random.Range(-10.16f, -13.7f), 0), Quaternion.identity);

        yield return new WaitForSeconds(Random.Range(2f, 7f));

        StartCoroutine(CreateTowers());

        yield return new WaitForEndOfFrame();
    }

    IEnumerator CreatePickups()
    {
        yield return new WaitForSeconds(Random.Range(2f, 7f));

        EZ_PoolManager.Spawn(pickup.transform, new Vector3(60f, Random.Range(7f, -7f), 0), Quaternion.identity);

        yield return new WaitForSeconds(Random.Range(1f, 2f));

        StartCoroutine(CreatePickups());

        yield return new WaitForEndOfFrame();
    }

    public void PlayerDied()
    {
        lives--;

        if (lives > 0)
        {
            StartCoroutine(PlayerRekt());
            livesText.text = lives.ToString();
        }

        else
            GameOver();
    }

    IEnumerator PlayerRekt()
    {
        isInvincible = true;

        player.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(.2f);
        player.GetComponent<SpriteRenderer>().enabled = true;

        isInvincible = false;

        yield return new WaitForEndOfFrame();
    }

    void GameOver()
    {
        youDied[Random.Range(0, youDied.Length)].DOFade(1f, 1f);
        gameOverScoreText.DOFade(1f, 1f);
        Invoke("RestartLevel", 6f);
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}