﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System;
using System.Collections.Generic;

public class AudioStuff : MonoBehaviour
{

    // EXPORTED VARIABLES
    public int final_note;              // This value is provided to the controller. 
    public static bool play_notes = false;     // When sound is above a certain threshold, enable the note gif on player. 
    public Text display;

    // Signal Processing Variables
    private float rmsValue;             // Root Mean Square
    private float dbValue;              // Amplitude in dB
    private float pitchValue;           // Main Frequency

    private int qSamples = 1024;
    private int binSize = 1024;             // Resolution (spectrum array size)
    private float refValue = 0.1f;
    private float threshold = 0.01f;        // min power threshold for frequencies at spectrum

    private List<Peak> peaks = new List<Peak>();
    float[] samples;                                    // Audio sample array (raw data)
    float[] spectrum;                                   // Output of Get Spectrum Data.
    int samplerate;                                     // Sampling rate for audio input via mic.

    // Moving Average - Smoothing Variables
    private int moving_idx = 0;
    private const int MAX_ARY = 20;
    private int[] pitch_array;

    // Use this for initialization
    void Start()
    {
        pitch_array = new int[MAX_ARY];
        for (int i = 0; i < MAX_ARY; i++)
        {
            pitch_array[i] = 0;
        }

        samples = new float[qSamples];
        spectrum = new float[binSize];
        samplerate = AudioSettings.outputSampleRate;    // ?

        // starts the Microphone and attaches it to the AudioSource
        GetComponent<AudioSource>().clip = Microphone.Start(null, true, 2, samplerate); // Default mic, loop, 10 seconds, given sample rate for input.
        GetComponent<AudioSource>().loop = true; // Set the AudioClip to loop
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the recording has started
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        AnalyzeSound();
        if (pitchValue > 50)
        {
            pitch_array[moving_idx] = (int)pitchValue;
            play_notes = true;
        }
        else {
            pitch_array[moving_idx] = final_note;
            play_notes = false;
        }
        moving_idx = (moving_idx + 1) % MAX_ARY;
        SmoothPitch();  // Average of the current array.
        if (display != null)
        {
            //display.text = "RMS: " + rmsValue.ToString ("F2") + " (" + dbValue.ToString ("F1") + " dB)\n" + "Pitch: " + final_note.ToString ("F0") + " Hz";
            display.text = "Pitch: " + final_note.ToString("F0") + " Hz";
        }
    }

    void AnalyzeSound()
    {
        GetComponent<AudioSource>().GetOutputData(samples, 0); // fill array with samples from channel 0.

        // GET RMS
        int i = 0;
        float sum = 0f;
        for (i = 0; i < qSamples; i++)
        {
            sum += samples[i] * samples[i];
        }
        rmsValue = Mathf.Sqrt(sum / qSamples); // rms = square root of average

        // CALCULATE AMPLITUDE
        dbValue = 20 * Mathf.Log10(rmsValue / refValue); // calculate dB
        if (dbValue < -160) dbValue = -160; // clamp it to -160dB min

        // get sound spectrum. actually you should have applied mel filterbank here.... anyways
        GetComponent<AudioSource>().GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
        float maxV = 0f;
        for (i = 0; i < binSize; i++)
        { // find max
            if ((spectrum[i] > maxV) && (spectrum[i] > threshold))
            {
                maxV = spectrum[i]; // Modify the max. tansu
                peaks.Add(new Peak(spectrum[i], i));
                if (peaks.Count > 5)
                { // get the 5 peaks in the sample with the highest amplitudes
                    peaks.Sort(new AmpComparer()); // sort peak amplitudes from highest to lowest
                    peaks.Remove(peaks[5]); // remove peak with the lowest amplitude
                }
            }
        }
        peaks.Sort(new AmpComparer()); //tansu

        float freqN = 0f;
        if (peaks.Count > 0)
        {
            //peaks.Sort (new IndexComparer ()); // sort indices in ascending order
            maxV = peaks[0].amplitude;
            int maxN = peaks[0].index;
            freqN = maxN; // pass the index to a float variable
            if (maxN > 0 && maxN < binSize - 1)
            { // interpolate index using neighbours
                var dL = spectrum[maxN - 1] / spectrum[maxN];
                var dR = spectrum[maxN + 1] / spectrum[maxN];
                freqN += 0.5f * (dR * dR - dL * dL);
            }
        }
        pitchValue = freqN * (samplerate / 2f) / binSize; // convert index to frequency
        peaks.Clear();
    }

    void SmoothPitch()
    {
        int sum = 0;
        for (int i = 0; i < MAX_ARY; i++)
        {
            sum += pitch_array[i];
        }
        final_note = sum / MAX_ARY;
    }
}// end of class AudioStuff


[RequireComponent(typeof(AudioSource))]
class Peak
{
    public float amplitude;
    public int index;

    public Peak()
    {
        amplitude = 0f;
        index = -1;
    }

    public Peak(float _frequency, int _index)
    {
        amplitude = _frequency;
        index = _index;
    }
}

class AmpComparer : IComparer<Peak>
{
    public int Compare(Peak a, Peak b)
    {
        return 0 - a.amplitude.CompareTo(b.amplitude);
    }
}

class IndexComparer : IComparer<Peak>
{
    public int Compare(Peak a, Peak b)
    {
        return a.index.CompareTo(b.index);
    }
}
